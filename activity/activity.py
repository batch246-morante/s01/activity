name = "Jose"
age = 38
occupation = "writer"
movie = "One More Chance"
rating = 99.6
# ---------------------------
num1 = 75
num2 = 2
num3 = 100
# ---------------------------
print(f"I am {name}, and I am {age} years old, I work as a {occupation}, and my rating for {movie} is {rating}%")
# print("Product of num1 and num2:")
print(num1 * num2)

# print("Is num1 less than num3?")
print(num1 < num3)

# print("Sum of num3 and num2:")
print(num3 + num2)
